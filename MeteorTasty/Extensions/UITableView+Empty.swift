//
//  UITableView+Empty.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 10.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit

extension UITableView {
    
    func displayEmptyMessage(message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.tastyBlack
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: R.font.praiseBrush.fontName, size: 25)!
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
}

