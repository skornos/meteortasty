//
//  ObjectMapper+Realm.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

// Based on: https://github.com/Jakenberg/ObjectMapper-Realm
public struct ListTransform<T: RealmSwift.Object>: TransformType where T: BaseMappable {
    
    public init() { }
    
    public typealias Object = List<T>
    public typealias JSON = Array<Any>
    
    public func transformFromJSON(_ value: Any?) -> List<T>? {
        if let objects = Mapper<T>().mapArray(JSONObject: value) {
            let list = List<T>()
            list.append(objectsIn: objects)
            return list
        }
        return nil
    }
    
    public func transformToJSON(_ value: Object?) -> JSON? {
        return value?.flatMap { $0.toJSON() }
    }
    
}

