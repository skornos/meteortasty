//
//  TastySwiftMessages.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 10.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import SwiftMessages

extension SwiftMessages {
    
    public static func configureMessage() -> Config {
        var config = SwiftMessages.Config()
        
        // Slide up from the bottom.
        config.presentationStyle = .top
        
        // Display in a window at the specified window level: UIWindowLevelStatusBar
        // displays over the status bar while UIWindowLevelNormal displays under.
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        
        // Disable the default auto-hiding behavior.
        config.duration = .seconds(seconds: 2)
        
        // Disable the interactive pan-to-hide gesture.
        config.interactiveHide = false
        
        // Specify a status bar style to if the message is displayed directly under the status bar.
        config.preferredStatusBarStyle = .lightContent
        
        return config
    }
    
    public static func configureView(body: String, theme: Theme) -> MessageView {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .statusLine)
        
        // Theme message elements with the warning style.
        view.configureTheme(theme)
        
        view.configureContent(body: body)
        
        return view
    }
    
    public static func showPendingMessage() {
        var config = SwiftMessages.configureMessage()
        // Keep displayed until manually dismissed and ensure that the message will
        // be displayed in most cases, but will be displayed for at least 2 second if
        // the operation takes longer than 1 seconds
        config.duration = .indefinite(delay: 0.2, minimum: 1)
        let view = SwiftMessages.configureView(body: "Fetching new meteors...", theme: .info)
        SwiftMessages.show(config: config, view: view)
    }
    
    public static func showSuccessMessage() {
        let config = SwiftMessages.configureMessage()
        let view = SwiftMessages.configureView(body: "Meteors successfully fetched.", theme: .success)
        view.backgroundView.backgroundColor = .tastyGreen
        // Hide the .forever duration message
        SwiftMessages.hideAll()
        SwiftMessages.show(config: config, view: view)
    }
    
    public static func showFailureMessage() {
        let config = SwiftMessages.configureMessage()
        let view = SwiftMessages.configureView(body: "Error while fetching meteors.", theme: .error)
        view.backgroundView.backgroundColor = .tastyBlack
        // Hide the .forever duration message
        SwiftMessages.hideAll()
        SwiftMessages.show(config: config, view: view)
    }
}

