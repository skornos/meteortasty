//
//  UINavigationController+BackBarButton.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 08.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit

extension UINavigationController {
    func removeLeftTitle() {
        if let topItem = navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
}

