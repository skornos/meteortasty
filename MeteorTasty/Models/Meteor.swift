//
//  Meteor.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import ObjectMapper
import RealmSwift
import CoreLocation
import MapKit

public protocol EndpointProtocol {
    func index() -> String
    static func endpointURLString() -> String
    static func endpointQuery() -> String
    static func paginationQuery(limit: Int, offset: Int) -> String
}

class Meteor: Object, StaticMappable, EndpointProtocol {
    @objc dynamic var fall: String        = ""
    @objc dynamic var location: Location?
    @objc dynamic var id: String          = ""
    @objc dynamic var mass: Double        = 0.0
    @objc dynamic var name: String        = ""
    @objc dynamic var nametype: String    = ""
    @objc dynamic var recclass: String    = ""
    @objc dynamic var reclat: String      = ""
    @objc dynamic var reclong: String     = ""
    @objc dynamic var year: String        = ""

    // StaticMappable required functions
    class func objectForMapping(map: Map) -> BaseMappable? {
        return Meteor()
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        fall        <- map["fall"]
        location    <- (map["geolocation"], LocationTransformType())
        mass        <- (map["mass"], DoubleTransformType())
        name        <- map["name"]
        nametype    <- map["nametype"]
        recclass    <- map["recclass"]
        reclat      <- map["reclat"]
        reclong     <- map["reclong"]
        year        <- map["year"]
    }
    
    // RealmSwift primary key
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // Endpoint Protocol
    static func endpointURLString() -> String {
        return "y77d-th95.json"
    }
    
    static func endpointQuery() -> String {
        return "?$where=year%20%3E=%20%272011-01-01T00:00:00%27&$order=mass%20DESC"
    }
    
    static func paginationQuery(limit: Int, offset: Int) -> String {
        return "&$limit=\(limit)&$offset=\(offset)"
    }
    
    func index() -> String {
        return self.id
    }
}

class Location: Object, StaticMappable {
    @objc dynamic var latitude: Double  = 0.0
    @objc dynamic var longitude: Double = 0.0
    
    // Meteor 1-1 inverse relationship
    let meteors = LinkingObjects(fromType: Meteor.self, property: "location")
    var meteor: Meteor? { return meteors.first }

    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(
            latitude: latitude,
            longitude: longitude)
    }
    
    class func objectForMapping(map: Map) -> BaseMappable? {
        return Location()
    }
    
    func mapping(map: Map) {
        latitude     <- map["coordinates"]
        longitude    <- map["coordinates"]
    }
}

class LocationTransformType: TransformType {
    
    public typealias Object = Location
    public typealias JSON = [String:Any]
    
    func transformFromJSON(_ value: Any?) -> Location? {
        if let locationDict = value as? [String: Any], let locationArray = locationDict["coordinates"] as? [Double], let longitude = locationArray[safe: 0], let latitude = locationArray[safe: 1] {
            let resultLocation = Location()
            resultLocation.latitude = latitude
            resultLocation.longitude = longitude
            return resultLocation
        }
        return nil
    }
    
    func transformToJSON(_ value: Location?) -> [String:Any]? {
        if let location = value {
            return [
                "coordinates": [
                    location.longitude,
                    location.latitude]
            ]
                
        }
        return nil
    }
}

class DoubleTransformType: TransformType {
    public typealias Object = Double
    public typealias JSON = String

    func transformFromJSON(_ value: Any?) -> Double? {
        return Double(value as! String)
    }
    
    func transformToJSON(_ value: Double?) -> String? {
        return value?.description
    }
}

// MARK: - MapKit displaying
extension Location: MKAnnotation {
    // coordinate already set
    // set other optional vars
}

extension Results where Element: Meteor {
    
    var annotations: [MKAnnotation] {
        return Array(self).map { meteor -> MKAnnotation? in
            return meteor.location
        }.flatMap{ $0 }
    }
}


