//
//  AppDelegate.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: PluggableApplicationDelegate {
    
    override var services: [ApplicationService] {
        return [
            AppearanceService(),
            LoggerService(),
            BackgroundFetchService(),
            DependencyInjectionService(window: window)
        ]
    }
}

