//
//  MeteorsTableViewController.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit
import RealmSwift
import PromiseKit
import SwiftMessages

class ListViewController: UITableViewController {
    
    // MARK: Dependency injection
    var APIManager: APIProtocol?
    var realmManager: RealmProtocol?

    // MARK: - IBOutlets
    @IBOutlet weak var searchFooter: SearchFooter!
    
    // MARK: - DataSource
    var meteorResults: Results<Meteor>?                 // List of meteors obtained from API
    var meteorSelected: Meteor?                         // Selected meteor for segue
    var meteorsNotificationToken: NotificationToken?    // Realm collection observing while the ViewController exists
    var meteorResultsFiltered: Results<Meteor>?

    // MARK: - UISearchController
    let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI
        setupNavigationController()
        setupSearchController()
        setupPullToRefresh()
        setupLargeTitleHiding()
        
        // DataSource
        checkDependecyInjection()
        setupDataSource()
        updateDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        meteorsNotificationToken?.invalidate()
    }
    
    // MARK: - UI
    func setupNavigationController() -> Void {
        // Remove left item title
        navigationController?.removeLeftTitle()
    }
    
    func setupSearchController() -> Void {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search in "
        searchController.definesPresentationContext = true
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.searchBarStyle = .minimal
        
        // More appearance is set within AppearanceService
        let searchBar = searchController.searchBar
        
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.blue
            if let backgroundview = textfield.subviews.first {
                // Background color
                backgroundview.backgroundColor = UIColor.white
                // Rounded corner
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    func setupPullToRefresh() {
        refreshControl?.addTarget(self, action: #selector(updateDataSource), for: .valueChanged)
    }
    
    func setupLargeTitleHiding() {
        // fix for automatically hiding Large title on scroll - Interface Builder values is ignored
        navigationItem.largeTitleDisplayMode = .automatic
    }
}

// Dependency Injection
private typealias ViewDependencyInjection = ListViewController
extension ViewDependencyInjection: DependencyInjectionProtocol {
    func inject(APIManager: APIProtocol) {
        self.APIManager = APIManager
    }
    
    func inject(realmManager: RealmProtocol) {
        self.realmManager = realmManager
    }
    
    func checkDependecyInjection() {
        guard APIManager != nil else { fatalError("APIManager should have been injected before view loads.") }
        guard realmManager != nil else { fatalError("realmManager should have been injected before view loads.") }
    }
}

// MARK: - DataSource
private typealias TableViewDataSource = ListViewController
extension TableViewDataSource  {
    private func setupDataSource() {
        firstly {
            RealmManager.shared.queryAll(realmType: Meteor.self)
        }.then { meteorResults -> Void in
            self.meteorResults = meteorResults?.sorted(byKeyPath: "mass", ascending: false)
            self.meteorsNotificationToken = meteorResults?.observe { changes in
                switch changes {
                case .initial:
                    self.tableView.reloadData()
                    
                case .update(_, let del, let ins, let upd):
                    if self.isFiltering() {
                        self.tableView.reloadData()
                    }
                    else {
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: ins.map {IndexPath(row: $0, section: 0)}, with: .automatic)
                        self.tableView.reloadRows(at: upd.map {IndexPath(row: $0, section: 0)}, with: .automatic)
                        self.tableView.deleteRows(at: del.map {IndexPath(row: $0, section: 0)}, with: .automatic)
                        self.tableView.endUpdates()
                    }
                    
                default: break
                }
            }
        }.catch { error in
            assert(false, "Realm ran out of memory")
        }
    }
    
    @objc private func updateDataSource() {
        SwiftMessages.showPendingMessage()
        APIManager?.getAllCollection(realmType: Meteor.self, limit: 500, offset: 0).always {
            SwiftMessages.hideAll()
            self.refreshControl?.endRefreshing()
        }.catch { error in
            SwiftMessages.showFailureMessage()
        }
    }

    // MARK: - Table view data source & delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        searchFooter?.setNotFiltering()
        if let meteors = meteorResults {
            // Is the search ON?
            if isFiltering(), let meteorResultsFiltered = meteorResultsFiltered {
                searchFooter?.setIsFilteringToShow(filteredItemCount: meteorResultsFiltered.count, of: meteors.count)
                return meteorResultsFiltered.count
            }
            return meteors.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isFiltering() ? 44.0 : 0.1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return searchFooter
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.meteorCell, for: indexPath)
        let meteor = isFiltering() ? meteorResultsFiltered![indexPath.row] : meteorResults![indexPath.row]
        
        cell?.textLabel?.text = meteor.name
        cell?.detailTextLabel?.text = meteor.mass.description
        
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        meteorSelected = isFiltering() ? meteorResultsFiltered![indexPath.row] : meteorResults![indexPath.row]
        performSegue(withIdentifier: R.segue.listViewController.mapSegue, sender: self)
    }
}

// MARK: - UISearchResultsUpdating Delegate
private typealias TableViewSearchController = ListViewController
extension TableViewSearchController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        do {
            // Filter contacts with the searchText - case insensitive
            let realm = try Realm()
            self.meteorResultsFiltered = realm.objects(Meteor.self).filter("name CONTAINS[c] %@", searchText)
            // Refresh the table
            self.tableView.reloadData()
        }
        catch {
            assert(false, "Realm ran out of memory")
        }
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
}

// MARK: - Navigation
private typealias Navigation = ListViewController
extension Navigation {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == R.segue.listViewController.mapSegue.identifier {
            if let toViewController = segue.destination as? MapViewController {
                toViewController.meteorSelected = meteorSelected    // Pass selected data
                meteorSelected = nil    // Clear previously selected data
            }
        }

    }
}

