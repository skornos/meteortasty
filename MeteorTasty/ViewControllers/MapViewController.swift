//
//  ViewController.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit
import MapKit
import PromiseKit
import RealmSwift

class MapViewController: UIViewController {

    // MARK: Realm Manager
    var realmManager: RealmProtocol?
    
    // MARK: - DataSource
    var meteorResults: Results<Meteor>?                 // List of meteors obtained from Realm
    var meteorSelected: Meteor?                         // Selected meteor for zooming in
    var meteorsNotificationToken: NotificationToken?    // Realm collection observing while the ViewController exists

    // MARK: - IBOutlets
    @IBOutlet private weak var mapView: MKMapView!
    
    // MARK: - MapView help properties
    var regionRadius: CLLocationDistance = 5000000
    var destinationRegion: MKCoordinateRegion?
    var intermediateAnimation = false
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup
        setupMapView()
        setupDataSource()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Process data passed by segue
        processSegue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Setup
    func setupMapView() {
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.register(MeteorAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    }
}

// Dependency Injection
private typealias ViewDependencyInjection = MapViewController
extension ViewDependencyInjection: DependencyInjectionProtocol {
    func inject(APIManager: APIProtocol) {
        // Not using API in this ViewController
    }

    func inject(realmManager: RealmProtocol) {
        self.realmManager = realmManager
    }
    
    func checkDependecyInjection() {
        guard realmManager != nil else { fatalError("realmManager should have been injected before view loads.") }
    }
}

// DataSource
private typealias ViewDataSource = MapViewController
extension ViewDataSource  {
    private func setupDataSource() {
        firstly {
            RealmManager.shared.queryAll(realmType: Meteor.self)
        }.then { meteorResults -> Void in
            self.meteorResults = meteorResults
            self.meteorsNotificationToken = meteorResults?.observe { changes in
                    switch changes {
                    case .initial(let results):
                        self.mapView.addAnnotations(results.annotations)
                        
                    case .update(let results, _, _, _):
                        self.mapView.removeAnnotations(self.mapView.annotations)
                        self.mapView.addAnnotations(results.annotations)
    
                    default: break
                    }
            }
        }.catch { error in
            assert(false, "Realm ran out of memory")
        }
    }
}


// MARK: - MapKit
private typealias MapViewDelegate = MapViewController
extension MapViewDelegate: MKMapViewDelegate {
    
    // MARK: - Helper functions
    private func mapRect(forCoordinateRegion region: MKCoordinateRegion) -> MKMapRect {
        let topLeft = CLLocationCoordinate2D(latitude: region.center.latitude + (region.span.latitudeDelta/2), longitude: region.center.longitude - (region.span.longitudeDelta/2))
        let bottomRight = CLLocationCoordinate2D(latitude: region.center.latitude - (region.span.latitudeDelta/2), longitude: region.center.longitude + (region.span.longitudeDelta/2))
        
        let a = MKMapPointForCoordinate(topLeft)
        let b = MKMapPointForCoordinate(bottomRight)
        
        return MKMapRect(origin: MKMapPoint(x:min(a.x,b.x), y:min(a.y,b.y)), size: MKMapSize(width: abs(a.x-b.x), height: abs(a.y-b.y)))
    }

    func centerMap(on region: MKCoordinateRegion) {
        var region = region
        var rect = mapRect(forCoordinateRegion: region)
        let intersection = MKMapRectIntersection(rect, mapView.visibleMapRect)
        
        if MKMapRectIsNull(intersection) {
            rect = MKMapRectUnion(rect, mapView.visibleMapRect)
            region = MKCoordinateRegionForMapRect(rect)
            intermediateAnimation = true
        }
        
        mapView.setRegion(region, animated: true)
    }
    
    // MARK: - MKMapViewDelegate
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if intermediateAnimation, let region = destinationRegion {
            intermediateAnimation = false
            mapView.setRegion(region, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let cluster = annotation as? MKClusterAnnotation {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier, for: annotation) as? MKMarkerAnnotationView
            annotationView?.glyphText = String(cluster.memberAnnotations.count)
            annotationView?.markerTintColor = UIColor.tastyOrange
            return annotationView
        } else if let location = annotation as? Location {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier, for: location) as? MKMarkerAnnotationView
            return annotationView
        } else {
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // Display all the member annotations on cluster tap
        if let cluster = view.annotation as? MKClusterAnnotation {
            mapView.showAnnotations(cluster.memberAnnotations, animated: true)
        }
    }
}

// MARK: - Navigation
private typealias Navigation = MapViewController
extension Navigation {
    
    func processSegue() {
        // Center map
        var initialCoordinates = CLLocationCoordinate2D(latitude: 49.195061, longitude: 16.606836)
        
        if let meteor = meteorSelected, let coordinate = meteor.location?.coordinate {
            if coordinate.latitude == 0, coordinate.longitude == 0 {
                ErrorManager.showAlert(withError: TastyError.mapFailed(reason: .unknownCoordinates))
            }
            else {
                regionRadius = 35000
                initialCoordinates = coordinate
            }
        }
        destinationRegion = MKCoordinateRegionMakeWithDistance(initialCoordinates, regionRadius * 2.0, regionRadius * 2.0)
        centerMap(on: destinationRegion!)
    }
}


