//
//  MeteorAnnotationView.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 09.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import MapKit

//  MARK: Annotation View
internal final class MeteorAnnotationView: MKMarkerAnnotationView {
    //  MARK: Properties
    internal override var annotation: MKAnnotation? { willSet { newValue.flatMap(configure(with:)) } }
}

//  MARK: Configuration
private extension MeteorAnnotationView {
    func configure(with annotation: MKAnnotation) {
        guard annotation is Location else { fatalError("Unexpected annotation type: \(annotation)") }
        markerTintColor = .tastyOrange
        glyphImage = #imageLiteral(resourceName: "Meteor")
        clusteringIdentifier = MKMapViewDefaultClusterAnnotationViewReuseIdentifier
    }
}

