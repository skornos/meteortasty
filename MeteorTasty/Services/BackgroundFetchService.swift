//
//  BackgroundFetchService.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 10.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit
import PromiseKit

final class BackgroundFetchService: NSObject, ApplicationService {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        let timeInterval: TimeInterval = 60 * 60 * 24 // 24 hours
        application.setMinimumBackgroundFetchInterval(timeInterval)
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Run the request on the background thread
        DispatchQueue.global(qos: .background).promise {
            APIManager.shared.getAllCollection(realmType: Meteor.self)
        }.then { _ in
            completionHandler(.newData)
        }.catch {_ in 
            completionHandler(.failed)
        }
    }
}
