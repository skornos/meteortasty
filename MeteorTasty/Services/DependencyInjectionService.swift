//
//  LoggerService.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit

public protocol DependencyInjectionProtocol {
    func inject(APIManager: APIProtocol)
    func inject(realmManager: RealmProtocol)
    func checkDependecyInjection()
}

final class DependencyInjectionService: NSObject, ApplicationService {
    
    var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        if let navigationViewController = window?.rootViewController as? UINavigationController, let rootViewController = navigationViewController.viewControllers.first as? DependencyInjectionProtocol {
            rootViewController.inject(APIManager: APIManager.shared)
            rootViewController.inject(realmManager: RealmManager.shared)
        }
        return true
    }
}

