//
//  LoggerService.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import Foundation
import SwiftyBeaver

let log = SwiftyBeaver.self

final class LoggerService: NSObject, ApplicationService {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        /* Configure SwiftyBeaver for logging */
        let console = ConsoleDestination() // log to Xcode Console
        // use custom format and set console output to short time, log level & message
        console.format = "$Dyyyy-MM-dd HH:mm:ss.SSSSSSSSSSS $d $C$L$c: $N.$F:$l - $M"
        // or use this for JSON output: console.format = "$J"
        // add the destinations to SwiftyBeaver
        log.addDestination(console)
        
        return true
    }
}

