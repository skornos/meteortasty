//
//  AppearanceService.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit

final class AppearanceService: NSObject, ApplicationService {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        // NavigationController appearance
        setupNavigationControllerAppearance()
        // SearchController appearance
        setupSearchControllerAppearance()
        // Refresh control
        setupRefreshControlAppearance()

        return true
    }
    
    private func setupNavigationControllerAppearance() {
        let navigationBarAppearace = UINavigationBar.appearance()
        // Status bar text to white
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
        // Back indicator image, button titles, button images color
        navigationBarAppearace.tintColor = UIColor.white
        // Bar background color
        navigationBarAppearace.barTintColor = UIColor.tastyOrange
        // Navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
//        navigationBarAppearace.prefersLargeTitles = false
        navigationBarAppearace.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont(name: R.font.praiseBrush.fontName, size: 55)!]
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont(name: R.font.praiseBrush.fontName, size: 25)!]
    }
            
    private func setupSearchControllerAppearance() {
        let searchBarAppearace = UISearchBar.appearance()
        searchBarAppearace.tintColor = .tastyOrange
        searchBarAppearace.barTintColor = .white
        
        // Re-color Cancel button
        UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.white

        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = .white
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGray])
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.darkGray]
    }
    
    private func setupRefreshControlAppearance() {
        let refreshControlAppearace = UIRefreshControl.appearance()
        refreshControlAppearace.tintColor = .white
    }
}


