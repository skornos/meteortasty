//
//  RealmManager.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 09.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import UIKit
import RealmSwift
import PromiseKit

public protocol RealmProtocol {
    func queryFirst<T> (realmType: T.Type) -> Promise<T?> where T: Object
    func queryAll<T> (realmType: T.Type) -> Promise<Results<T>?> where T: Object
    func write<T> (collection: [T], deleteDifferences: Bool) -> Promise<Void> where T: Object, T:EndpointProtocol
}

final class RealmManager: RealmProtocol {    
    // MARK: - Singletons
    static let shared = RealmManager()
    
    // MARK: - Init
    private init() {
    }
    
    // MARK: - Find first object of the given type
    func queryFirst<T> (realmType: T.Type) -> Promise<T?> where T: Object {
        return Promise { fulfill, _ in
            let realm = try RealmProvider.realm()
            let result = realm.objects(T.self).first
            fulfill(result)
        }
    }

    // MARK: - Find all objects of the given type
    func queryAll<T> (realmType: T.Type) -> Promise<Results<T>?> where T: Object {
        return Promise { fulfill, _ in
            let realm = try RealmProvider.realm()
            let results = realm.objects(T.self)
            fulfill(results)
        }
    }
    
    @discardableResult
    func write<T> (collection: [T], deleteDifferences: Bool) -> Promise<Void> where T: Object, T:EndpointProtocol {
        return Promise { fulfill, _ in
            let realm = try RealmProvider.realm()
            try realm.write {
                // Persist the collection
                var collectionIDs: [String] = []
                for item in collection {
                    realm.add(item, update: true)
                    collectionIDs.append(item.index())
                }
                // Delete all objects but the collection
                if deleteDifferences, let primaryKey = T.primaryKey() {
                    let predicate = NSPredicate(format: "NOT \(primaryKey) IN %@", collectionIDs)
                    realm.delete(realm.objects(T.self).filter(predicate))
                }
            }
            fulfill(())
        }
    }
}

// Class for providing Realm for the current context - e.g. in memory DB for UnitTesting.
// In memory DB doesn't interfere with the production DB.
final class RealmProvider {
    class func realm() throws -> Realm {
        if let _ = NSClassFromString("XCTest") {
            return try Realm(configuration: Realm.Configuration(fileURL: nil, inMemoryIdentifier: "inMemoryDB", encryptionKey: nil, readOnly: false, schemaVersion: 0, migrationBlock: nil, objectTypes: nil))
        } else {
            return try Realm()
            
        }
    }
}
