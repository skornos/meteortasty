//
//  APIManager.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import Foundation
import PromiseKit
import ObjectMapper
import SwiftyJSON
import RealmSwift

public protocol APIProtocol {
    func getCollection<T> (realmType: T.Type, limit: Int, offset: Int, deleteDifferences: Bool) -> Promise<Int> where T: BaseMappable, T: Object, T:EndpointProtocol
    func getAllCollection<T> (realmType: T.Type, limit: Int, offset: Int) -> Promise<Int> where T: BaseMappable, T: Object, T:EndpointProtocol
}

public class APIManager: APIProtocol {
    
    // MARK: - Singletons
    static let shared = APIManager()
    var networkManager: NetworkManager!
    var realmManager: RealmManager!

    // MARK: - Endpoint
    var accessToken: String
    var baseURLString: String
    
    // MARK: - Init
    private init() {
        networkManager = NetworkManager.shared
        realmManager = RealmManager.shared
        accessToken = "1ICffGfjtPI26oLFFR2uQA5sR"
        baseURLString = "https://data.nasa.gov/resource/"
    }
    
    // MARK: - PromiseKit retry function
    // Usage: retry(times: 2, cooldown: 10) { APIManager.shared.someCall().then { } }
    func retry<T>(times: Int, cooldown: TimeInterval, body: @escaping () -> Promise<T>) -> Promise<T> {
        var retryCounter = 0
        func attempt() -> Promise<T> {
            return body().recover(policy: CatchPolicy.allErrorsExceptCancellation) { error -> Promise<T> in
                retryCounter += 1
                guard retryCounter <= times else {
                    throw error
                }
                return after(interval: cooldown).then(execute: attempt)
            }
        }
        return attempt()
    }
    
    // MARK: - Generics function to get objects from API
    @discardableResult
    public func getCollection<T> (realmType: T.Type, limit: Int = 500, offset: Int = 0, deleteDifferences: Bool) -> Promise<Int> where T: BaseMappable, T: Object, T:EndpointProtocol {
        return
            firstly { () -> Promise<String> in
                let headers: HTTPHeaders = [
                    "X-App-Token": accessToken,
                    "Accept": "application/json"
                ]
                let endpointURL = baseURLString + T.endpointURLString() + T.endpointQuery() + T.paginationQuery(limit: limit, offset: offset)
                return networkManager.getData(fromURL: endpointURL, headers: headers)
            }.then { stringJSON -> Int in
                // Create the ObjectMapper object from JSON
                guard let jsonArray = JSON(parseJSON: stringJSON).arrayObject as? [[String: Any]] else {
                    log.error("Error while parsing response object as array: \(T.Type.self)")
                    throw TastyError.apiFailed(reason: .parsingFailed)
                }
                let responseCollection = Mapper<T>().mapArray(JSONArray: jsonArray)
                // Persist the data with Realm
                self.realmManager.write(collection: responseCollection, deleteDifferences: deleteDifferences)
                // Return the number of the results from API
                return responseCollection.count
            }.catch { error in
                ErrorManager.showAlert(withError: error)
            }
    }
    
    @discardableResult
    public func getAllCollection<T> (realmType: T.Type, limit: Int = 500, offset: Int = 0) -> Promise<Int> where T: BaseMappable, T: Object, T:EndpointProtocol {
        return
            APIManager.shared.getCollection(realmType: T.self, limit: limit, offset: offset, deleteDifferences: false).then { responseCollectionCount -> Promise<Int> in
                if responseCollectionCount <= 0 {
                    // throw Cancelable error
                    throw TastyErrorCancelable.cancelled
                }
                else {
                    return APIManager.shared.getAllCollection(realmType: T.self, limit: limit, offset: offset + 500)
                }
            }

    }
}

