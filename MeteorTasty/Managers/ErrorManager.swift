//
//  ErrorManager.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import PromiseKit

// MARK: - Cancelable TastyError for PromiseKit

public enum TastyErrorCancelable: CancellableError {
    /// Cancelled by inner logic. Don't show error message
    case cancelled
    /// - Returns: true
    public var isCancelled: Bool {
        switch self {
        case .cancelled:
            return true
        }
    }
}

public enum TastyError: Error {
    public enum APIFailureReason {
        case parsingFailed
    }
    public enum MapFailureReason {
        case unknownCoordinates
    }
    
    case apiFailed(reason: APIFailureReason)
    case mapFailed(reason: MapFailureReason)
}

// MARK: - TastyError localized descriptions

extension TastyError.APIFailureReason {
    var localizedError: String {
        switch self {
        case .parsingFailed:
            return "Object parsing failed."
        }
    }
}

extension TastyError.MapFailureReason {
    var localizedError: String {
        switch self {
        case .unknownCoordinates:
            return "Unable to display the coordinates."
        }
    }
}

// MARK: - Convenience Properties

extension TastyError {
    public var localizedError: String {
        switch self {
        case .apiFailed(let reason):
            return reason.localizedError
        case .mapFailed(let reason):
            return reason.localizedError
        }
    }
}

// MARK: - ErrorManages for handling the general errors and displaying the error alert messages

final class ErrorManager {
    
    static func getErrorMessage(fromError error:Error) -> String {
        var message: String
        
        if let error = error as? AFError {
            switch error {
            case .invalidURL(let url):
                message = "Invalid URL: \(url) - \(error.localizedDescription)"
            case .parameterEncodingFailed(let reason):
                message = "Parameter encoding failed: \(error.localizedDescription).\nFailure Reason: \(reason)."
            case .multipartEncodingFailed(let reason):
                message = "Multipart encoding failed: \(error.localizedDescription).\nFailure Reason: \(reason)."
            case .responseValidationFailed(let reason):
                message = "Response validation failed: \(error.localizedDescription).\nFailure Reason: \(reason)."
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    message = message + "\nDownloaded file could not be read."
                case .missingContentType(let acceptableContentTypes):
                    message = message + "\nContent Type Missing: \(acceptableContentTypes)."
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    message = message + "\nResponse content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)."
                case .unacceptableStatusCode(let code):
                    message = message + "\nResponse status code was unacceptable: \(code)."
                }
            case .responseSerializationFailed(let reason):
                message = "Response serialization failed: \(error.localizedDescription).\nFailure Reason: \(reason)."
            }
            
            message = message + "\nUnderlying error: \(String(describing: error.underlyingError))."
        } else if let error = error as? URLError {
            message = "URLError occurred: \(error)."
        } else if let error = error as? TastyError {
            message = "\(String(describing: error.localizedError))"
        } else {
            message = "Unknown error: \(String(describing: error))."
        }
        return message
    }
    
    static func createAlertController(withMessage message: String) -> UIAlertController {
        // create alert controller
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
        
        return alertController
    }
    
    static func showAlert(withError error:Error) {
        // get error message from Error class
        let message = ErrorManager.getErrorMessage(fromError: error)
        let alertController = ErrorManager.createAlertController(withMessage: message)
        
        // get top view controller
        let topController = UIApplication.topViewController()
        // iPad compatibility
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = topController?.view
            popoverController.sourceRect = (topController?.view.bounds)!
        }
        
        topController?.present(alertController, animated: true, completion: nil)
    }
}

