//
//  NetworkManager.swift
//  MeteorTasty
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireLogger
import PromiseKit

final class NetworkManager {
    
    private init() { }
    
    static let shared = NetworkManager()

    // MARK: - GET
    final func getData(fromURL url:URLConvertible, headers: HTTPHeaders) -> Promise<String> {
        if Thread.isMainThread {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        return
            firstly {
                Alamofire.request(url, method: .get, headers: headers)
                    .log(.verbose)
                    .validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"])
                    .responseString()
            }.always {
                if Thread.isMainThread {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
    }
}

