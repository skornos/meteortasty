//
//  MeteorTastyTests.swift
//  MeteorTastyTests
//
//  Created by Petr Škorňok on 07.12.17.
//  Copyright © 2017 Petr Skornok. All rights reserved.
//

import XCTest
import SwiftyJSON
import ObjectMapper
import PromiseKit
import RealmSwift
@testable import MeteorTasty

class MeteorTastyTests: XCTestCase {
    
    // MARK: - Meteors Data Source
    var meteorsFileName = "UnitTestData"
    var meteorsFromJSON: [Meteor] = []
    var meteorsFromJSONSlice: [Meteor] = []
    
    // MARK: - Realm manager
    var realmManager: RealmProtocol?
    
    override func setUp() {
        super.setUp()
        guard let pathString = Bundle(for: type(of: self)).path(forResource: meteorsFileName, ofType: "json") else {
            fatalError("\(meteorsFileName).json not found")
        }
        
        guard let stringJSON = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert \(meteorsFileName).json to String")
        }
        
        guard let jsonArray = JSON(parseJSON: stringJSON).arrayObject as? [[String: Any]] else {
            fatalError("Invalid JSON within \(meteorsFileName).json")
        }
        
        meteorsFromJSON = Mapper<Meteor>().mapArray(JSONArray: jsonArray)
        realmManager = RealmManager.shared
        
        // End the test after first failure
        continueAfterFailure = false
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testJSONLoading() {
        // JSON File successfully loaded
        XCTAssertNotNil(meteorsFromJSON)
        // JSON File contains valid data
        XCTAssertTrue(meteorsFromJSON.count > 0)
    }
    
    func testRealmManagerExists() {
        // Realm manager not nil
        XCTAssertNotNil(realmManager)
    }
    
    func testRealmManagerWriteCollection() {
        // Test adding new objects to Realm
        firstly { () -> Promise<Void> in
            realmManager!.write(collection: meteorsFromJSON, deleteDifferences: true)
        }.then { _ -> Promise<Results<Meteor>?> in
            self.realmManager!.queryAll(realmType: Meteor.self)
        }.then { meteorResults in
            XCTAssertEqual(self.meteorsFromJSON.count, meteorResults?.count)
        }
    }
    
    func testRealmManagerDeleteDifferences() {
        meteorsFromJSONSlice = Array(meteorsFromJSON[0..<meteorsFromJSON.count-1])
        // Test adding new objects to Realm
        firstly { () -> Promise<Void> in
            return realmManager!.write(collection: meteorsFromJSON, deleteDifferences: true)
        }.then { _ -> Promise<Results<Meteor>?> in
            self.realmManager!.queryAll(realmType: Meteor.self)
        }.then { meteorResults in
            XCTAssertEqual(self.meteorsFromJSONSlice.count, meteorResults?.count)
        }
    }
}
